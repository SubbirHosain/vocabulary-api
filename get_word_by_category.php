<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 9/24/2018
 * Time: 1:04 AM
 */

//required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

//include database
include 'db/database.php';
$dbh = new Database();

if (!empty($_GET['word_category_id'])) {

    $word_category_id = intval($_GET['word_category_id']);
    $sql = "SELECT word_id,english_word,bangla_meaning,english_example,word_category_id FROM word WHERE word_category_id=? ORDER BY word_id DESC ";
    $id = array($word_category_id);

    if ($dbh->rowCounts($sql,$id) > 0) {

        // vocabulary array
        $vocabularies_arr = array();
        $vocabularies_arr["vocabularies"] = array();
        $vocabularies_arr['status'] = true;

        $result = $dbh->getRows($sql,$id);
        foreach ($result as $row) {
            extract($row);
            $vocabulary_item = array(
                "word_id" => intval($word_id),
                "english_word" => $english_word,
                "bangla_meaning" => $bangla_meaning,
                "english_example" => $english_example,
                "word_category_id" => intval($word_category_id)
            );
            array_push($vocabularies_arr["vocabularies"], $vocabulary_item);
        }
        echo json_encode($vocabularies_arr, JSON_UNESCAPED_UNICODE);
    } else {
        echo json_encode(
            array("message" => "No vocabularies found.")
        );
    }
}

