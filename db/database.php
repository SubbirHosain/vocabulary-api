<?php

class Database
{
    private $isConn;
    private $db_name = 'subbirho_vocabulary';
    private $db_user = 'subbirho_voca';
    private $db_pass = '';
    private $db_host = 'localhost';
    private $char = 'utf8';
    private $dbh;

    public function __construct()
    {
        if (!isset($this->dbh)) {

            $dsn = 'mysql:host=' . $this->db_host . ';dbname=' . $this->db_name . ';charset=' . $this->char;

            $options = array(PDO::ATTR_PERSISTENT => true);

            try {
                $link = new PDO($dsn, $this->db_user, $this->db_pass, $options);
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                $this->dbh = $link;
                $this->isConn = TRUE;
            } catch (PDOException $exc) {
                $errmsg = $exc->getMessage();
                echo "{'status':400,'message':$errmsg}";
            }
        }
    }

    public function rowCounts($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->rowCount();
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }

    public function getRow($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }


    public function getRows($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }


    public function insertRow($query, $params = [])
    {

        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return TRUE;
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }

    public function updateRow($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return $sth->rowCount();
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }

    public function deleteRow($query, $params = [])
    {
        try {
            $sth = $this->dbh->prepare($query);
            $sth->execute($params);
            return TRUE;
        } catch (PDOException $exc) {
            $errmsg = $exc->getMessage();
            echo "{'status':400,'message':$errmsg}";
        }
    }


} ?>