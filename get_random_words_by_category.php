<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 9/27/2018
 * Time: 11:55 AM
 */

include "db/database.php";
$dbh = new Database();

if (!empty($_GET['word_category_id'])){

    $word_category_id = $_GET['word_category_id'];
    $sql = "SELECT word_id,english_word,bangla_meaning,english_example FROM word WHERE word_category_id=? ORDER BY RAND() LIMIT 20";
    $id = array($word_category_id);

    $result = $dbh->getRows($sql,$id);
    $data['vocabularies'] = $result;

    header("HTTP/1.1 200 OK");
    header("Content-Type: application/json;charset=utf-8");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}