<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 9/24/2018
 * Time: 12:52 AM
 */
include "db/database.php";
$dbh = new Database();

$sql = "SELECT * FROM word_category";

$data = $dbh->getRows($sql);

header("HTTP/1.1 200 OK");
header("Content-Type: application/json;charset=utf-8");
echo json_encode($data,JSON_UNESCAPED_UNICODE);