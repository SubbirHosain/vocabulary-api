<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 9/27/2018
 * Time: 1:16 AM
 */

include "db/database.php";
$dbh = new Database();

$sql = "SELECT word_id,english_word,bangla_meaning,english_example FROM word ORDER BY RAND() LIMIT 20";
$result = $dbh->getRows($sql);
$data['vocabularies'] = $result;
header("HTTP/1.1 200 OK");
header("Content-Type: application/json;charset=utf-8");
echo json_encode($data, JSON_UNESCAPED_UNICODE);