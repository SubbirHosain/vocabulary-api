<?php
/**
 * Created by PhpStorm.
 * User: SubbirHosain
 * Date: 9/25/2018
 * Time: 12:21 AM
 */

include "db/database.php";
$dbh = new Database();

if (!empty($_GET['filter'])) {

    $filter_words = $_GET['filter'];

    switch ($filter_words) {
        case "Daily":
            //function to get the words by filter keyword
            $sql = "SELECT word_id,english_word,bangla_meaning,english_example FROM word WHERE CONVERT(word_created_at, DATE)= CURRENT_DATE ORDER BY word_id DESC";
            $result = $dbh->getRows($sql);
            $data['vocabularies'] = $result;
            header("HTTP/1.1 200 OK");
            header("Content-Type: application/json;charset=utf-8");
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
            break;
        case "Yesterday":
            //function to get the words by filter keyword
            $sql = "SELECT word_id,english_word,bangla_meaning,english_example FROM word WHERE DATE(word_created_at) = DATE(NOW() - INTERVAL 1 DAY) ORDER BY word_id DESC";
            $result = $dbh->getRows($sql);
            $data['vocabularies'] = $result;
            header("HTTP/1.1 200 OK");
            header("Content-Type: application/json;charset=utf-8");
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
            break;
        case "Weekly":
            //function to get the words by filter keyword
            $sql = "SELECT word_id,english_word,bangla_meaning,english_example FROM word WHERE word_created_at >= current_date - 7 ORDER BY word_id DESC";
            $result = $dbh->getRows($sql);
            $data['vocabularies'] = $result;
            header("HTTP/1.1 200 OK");
            header("Content-Type: application/json;charset=utf-8");
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
            break;
        case "Monthly":
            //function to get the words by filter keyword
            $sql = "SELECT word_id,english_word,bangla_meaning,english_example FROM word WHERE word_created_at >= current_date - 30 ORDER BY word_id DESC";
            $result = $dbh->getRows($sql);
            $data['vocabularies'] = $result;
            header("HTTP/1.1 200 OK");
            header("Content-Type: application/json;charset=utf-8");
            echo json_encode($data, JSON_UNESCAPED_UNICODE);
            break;
    }


}